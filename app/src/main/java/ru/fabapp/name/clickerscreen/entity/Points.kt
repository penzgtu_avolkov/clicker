package ru.fabapp.name.clickerscreen.entity

data class Points(
    var point: Int = 0
) {
    fun addPoint(): Int = ++point
    fun addPointValue(value: Int): Int {
        point += value
        return point
    }
}

package ru.fabapp.name.clickerscreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import ru.fabapp.name.R
import ru.fabapp.name.clickerscreen.entity.Level
import ru.fabapp.name.clickerscreen.entity.Points
import ru.fabapp.name.historyactivity.HistoryActivity
import ru.fabapp.name.settings.Buster
import ru.fabapp.name.settings.Settings

class MainActivity : AppCompatActivity() {

    private var txtCounter: TextView? = null
    private var btnAdd: Button? = null
    private var btnClear: Button? = null
    private var tvLevel: TextView? = null
    private var btnSettings: Button? = null
    private var tvName: TextView? = null

    private val point = Points()
    private val level = Level()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        setListeners()

        tvLevel?.text = "Ваш уровень - 0"
        tvName?.text = intent.getStringExtra("NAME")
    }

    private fun initViews() {
        txtCounter = this.findViewById(R.id.tvCounter)
        btnAdd = this.findViewById(R.id.btnAdd)
        btnClear = this.findViewById(R.id.btnClear)
        tvLevel = this.findViewById(R.id.tvLevel)
        btnSettings = findViewById(R.id.btnSettings)
        tvName = findViewById(R.id.tvName)
    }

    private fun setListeners() {
        btnAdd?.setOnClickListener {
            txtCounter?.text = point.addPoint().toString()
            resolveLevel(point.point)
        }

        btnClear?.setOnClickListener {
            txtCounter?.text = "0"
            point.point = 0
            level.level = 0
            tvLevel?.text = "Ваш уровень - 0"
        }

        tvName?.setOnClickListener {
            startActivity(
                Intent(this, HistoryActivity::class.java)
            )
        }

        btnSettings?.setOnClickListener {
            val intent = Intent(this, Settings::class.java)
            intent.apply {
                putExtra("CLICK_VALUE", point.point)
            }
            startActivityForResult(intent, 123)
        }

    }

    private fun resolveLevel(point: Int) {
        val lvl = point / 10
        level.level = lvl
        tvLevel?.text = "Ваш уровень - ${level.level}"

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 123 && resultCode == RESULT_OK) {
            resolveBust(data?.getParcelableExtra("BUSTER"))
        }
    }

    private fun resolveBust(buster: Buster?) {
        when (buster) {
            is Buster.PointBuster -> addPoints(buster.bust)
            is Buster.ClickBuster -> setClickPoint(buster.bust)
        }
    }

    private fun setClickPoint(bust: Int) {
        if (bust == 2) {
            btnAdd?.text = "+2"
            btnAdd?.setOnClickListener {
                txtCounter?.text = point.addPointValue(2).toString()
                resolveLevel(point.point)
            }
        } else {
            btnAdd?.text = "+3"
            btnAdd?.setOnClickListener {
                txtCounter?.text = point.addPointValue(3).toString()
                resolveLevel(point.point)
            }
        }
    }

    private fun addPoints(bust: Int) {
        point.point += bust
        txtCounter?.text = point.point.toString()

        if (bust == 100) level.level += 10
        else level.level += 30

        tvLevel?.text = "Ваш уровень - ${level.level}"
    }
}

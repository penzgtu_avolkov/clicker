package ru.fabapp.name.loginactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ru.fabapp.name.R
import ru.fabapp.name.clickerscreen.MainActivity

class LoginActivity : AppCompatActivity() {

    var btnNext: Button? = null
    var edName: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnNext = findViewById(R.id.btnNext)
        edName = findViewById(R.id.edName)

        btnNext?.setOnClickListener {
            startActivity(
                Intent(this, MainActivity::class.java).
                        putExtra("NAME", edName?.text.toString())
            )
        }
    }
}
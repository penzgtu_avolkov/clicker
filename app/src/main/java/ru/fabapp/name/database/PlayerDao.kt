package ru.fabapp.name.database

import androidx.room.*

@Dao
interface PlayerDao {
    @Query("SELECT * FROM player")
    fun getPlayers(): List<Player>
    @Delete
    fun deletePlayer(player: Player)
    @Update
    fun updatePlayer(player: Player)
    @Insert
    fun insertPlayer(player: Player)
    @Query("SELECT * FROM player WHERE id = :id")
    fun getPlayersWithIds(id: List<Long>): List<Player>
}

package ru.fabapp.name.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Player(
    @PrimaryKey val id: Long,
    val nickName: String,
    val points: Int,
)

package ru.fabapp.name.settings

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

sealed class Buster: Parcelable {
    @Parcelize class ClickBuster(val bust: Int): Buster()
    @Parcelize class PointBuster(val bust: Int): Buster()
}



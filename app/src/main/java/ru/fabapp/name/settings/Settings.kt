package ru.fabapp.name.settings

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.core.view.isVisible
import ru.fabapp.name.R

class Settings : AppCompatActivity() {

    var btnAddTwo: Button? = null
    var btnAddThree: Button? = null
    var btnAddPoints100: Button? = null
    var btnAddPoints300: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        initViews()

        setListeners()

        resolveBusters()
    }

    private fun resolveBusters() {
        val points = intent.getIntExtra("CLICK_VALUE", -1)

        if (points < 0) {
            finish()
        }
            if (points > 21) btnAddTwo?.isVisible = true
            if (points > 31) btnAddThree?.isVisible = true
            if (points > 51) btnAddPoints100?.isVisible = true
            if (points > 71) btnAddPoints300?.isVisible = true

    }

    private fun setListeners() {
        btnAddTwo?.setOnClickListener {
            setResult(
                RESULT_OK,
                Intent().putExtra("BUSTER", Buster.ClickBuster(2))
            )
            finish()
        }
        btnAddThree?.setOnClickListener {
            setResult(
                RESULT_OK,
                Intent().putExtra("BUSTER", Buster.ClickBuster(3))
            )
            finish()
        }
        btnAddPoints100?.setOnClickListener {
            setResult(
                RESULT_OK,
                Intent().putExtra("BUSTER", Buster.PointBuster(100))
            )
            finish()
        }
        btnAddPoints300?.setOnClickListener {
            setResult(
                RESULT_OK,
                Intent().putExtra("BUSTER", Buster.PointBuster(300))
            )
            finish()
        }
    }

    private fun initViews() {
        btnAddTwo = findViewById(R.id.addTwo)
        btnAddThree = findViewById(R.id.addThree)
        btnAddPoints100 = findViewById(R.id.addPoints100)
        btnAddPoints300 = findViewById(R.id.addPoints300)
    }
}

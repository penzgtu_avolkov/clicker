package ru.fabapp.name.historyactivity

data class UserInfo(
    val name: String,
    val score: String,
    val level: String,
)

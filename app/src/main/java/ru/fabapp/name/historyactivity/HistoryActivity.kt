package ru.fabapp.name.historyactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.jetbrains.anko.doAsync
import ru.fabapp.name.App
import ru.fabapp.name.R
import ru.fabapp.name.database.AppDatabase
import ru.fabapp.name.database.Player
import ru.fabapp.name.database.PlayerDao
import ru.fabapp.name.userstory.UserStoryActivity

class HistoryActivity : AppCompatActivity(), RvUsersAdapter.OnListItemClick {

    var rvUsers: RecyclerView? = null

    lateinit var db: AppDatabase
    lateinit var playerDao: PlayerDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        db = App.getDatabase()
        playerDao = db.playerDao()
        val playres = getUsers()

//        insertPlayers(playres)

        setPlayersFromDb()

        rvUsers = findViewById(R.id.rvUsers)
        rvUsers?.layoutManager = LinearLayoutManager(this)
    }

    private fun setPlayersFromDb() {
        doAsync {
            val list = playerDao.getPlayers()
            runOnUiThread {
                rvUsers?.apply {
                    adapter = RvUsersAdapter(
                        list.sortedBy { it.points },
                        this@HistoryActivity
                    )
                }
            }
        }
    }

    private fun insertPlayers(playres: List<Player>) {
        doAsync {
            playres.forEach {
                playerDao.insertPlayer(it)
            }
        }
    }

    fun getUsers(): List<Player> {
        return listOf(
            Player(1, "Bob", 1),
            Player(2, "Alice", 4),
            Player(3, "Jack", 35),
            Player(4, "Nick", 144),
            Player(5, "Stan", 24),
            Player(6, "Pol", 14),
        )
    }

    override fun onClick(position: Int) {
        startActivity(
            Intent(this, UserStoryActivity::class.java).apply {
                putExtra("POSITION", position)
            }
        )
    }


}
package ru.fabapp.name.historyactivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.fabapp.name.R
import ru.fabapp.name.database.Player

class RvUsersAdapter(
    private val users: List<Player>,
    private val onListItemClick: OnListItemClick,
) :
    RecyclerView.Adapter<RvUsersAdapter.RvUsersViewHolder>() {

    interface OnListItemClick {
        fun onClick(position: Int)
    }

    class RvUsersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvUserName: TextView? = null
        var tvScore: TextView? = null

        init {
            tvUserName = itemView.findViewById(R.id.tvUserName)
            tvScore = itemView.findViewById(R.id.tvScore)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RvUsersViewHolder {
        val item = LayoutInflater.from(parent.context).inflate(
            R.layout.user_list_item, parent, false
        )

        return RvUsersViewHolder(item)
    }

    override fun onBindViewHolder(holder: RvUsersViewHolder, position: Int) {
        val currentUser = users[position]

        holder.tvScore?.text = currentUser.points.toString()
        holder.tvUserName?.text = currentUser.nickName

        holder.tvUserName?.setOnClickListener {
            onListItemClick.onClick(position)
        }
    }

    override fun getItemCount(): Int {
        return users.size
    }
}
package ru.fabapp.name.userstory

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback;
import retrofit2.Response
import ru.fabapp.name.R
import ru.fabapp.name.retrofit.RetrofitFactory
import ru.fabapp.name.retrofit.RickAndMortyApi
import ru.fabapp.name.retrofit.User

class UserStoryActivity : AppCompatActivity() {

    lateinit var tvName: TextView
    lateinit var tvGender: TextView
    lateinit var tvSpecies: TextView

    lateinit var ivPhoto: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_story_acticity)

        val id = intent.getIntExtra("POSITION", 0)

        tvName = findViewById(R.id.tvName)
        tvGender = findViewById(R.id.tvGender)
        tvSpecies = findViewById(R.id.tvSpecies)

        ivPhoto = findViewById(R.id.ivPhoto)

        val retrofit = RetrofitFactory.getRetrofit()

        val rickAndMortyApi = retrofit.create(RickAndMortyApi::class.java)

        doAsync {
            val user = rickAndMortyApi.getCharacter(id.plus(1)).execute().body()
            runOnUiThread {
                user?.let {
                    tvName.text = it.name
                    tvGender.text = it.gender
                    tvSpecies.text = it.species

                    Picasso.with(this@UserStoryActivity)
                        .load(user.image)
                        .into(ivPhoto)
                }
            }
        }
    }
}

package ru.fabapp.name.retrofit

data class User(
    val id: Int,
    val name: String,
    val species: String,
    val gender: String,
    val image: String,
)
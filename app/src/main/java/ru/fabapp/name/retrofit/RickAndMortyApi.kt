package ru.fabapp.name.retrofit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface RickAndMortyApi {
    @GET("character/{character}")
    fun getCharacter(
        @Path("character") character: Int,
    ): Call<User>
}

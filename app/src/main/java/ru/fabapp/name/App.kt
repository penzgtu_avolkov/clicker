package ru.fabapp.name

import android.app.Application
import androidx.room.Room
import ru.fabapp.name.database.AppDatabase

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        app = this
        db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "database")
            .build()
    }

    companion object {
        lateinit var app: App
        lateinit var db: AppDatabase

        fun getInstance() = app
        fun getDatabase() = db
    }

}